//Clase que muestra varios metodos de la clase String()

package Laboratorio3;

public class MisChars {
   /**
    * Programa que imprime Arriba/viva Colombia/México
    * @param args Argumentos de línea de comandos.
    *
    */

    public static void main(String[] args) throws Exception {
	
	//Declaracin de variables
	
	String expresion1, expresion2, expresion3, pais1, pais2;
	String banner, banner1, banner2, aux;
	
	int bandera, largo, i, num=-1;
	
	char[] letrero;
	
        if (args.length == 0) {
            System.out.println ("Uso: java MisChars [1/0]");
	    System.out.println ("Donde 1 es Maysculas y 0 es minsculas");
	}
        else {    
	    //leo los atributos de la linea de comandos y es los asigno a num
	    num = Integer.parseInt (args[0]);		
	    
	    //verifico el valor de num
	    if (num==1)
		expresion3 = "ABAJO";
	    else
		expresion3 = "arriba";
	    
	    banner1 = banner2 = aux = " ";
	    expresion1 = "vIVa";
	    expresion2 = "aRrIbA";	
	    
	    pais1 = "mexico";
	    pais2 = "colombia";
	    
	    
	    
	    bandera = expresion3.compareToIgnoreCase("ArRiBa");
	    
	    
	    
	    
	    System.out.println("La comparacin da como resultado " + bandera);
	    
	    if (bandera == 0) {
		    //Arriba colombia viva mxico
			banner1 = banner1 + expresion2.toUpperCase();  // a banner 1 le adiciono la expresin 2 (Arriba)
			banner1 = banner1 + " ";
			banner1 +=pais2;
			
			banner2 = banner2 + expresion1.substring(0,1);
			banner2 = banner2.toUpperCase();
			largo = expresion1.length();
			aux = expresion1.substring(1, largo);
			banner2 = banner2.concat(aux);
			banner2 = banner2 + " " + pais1;
	    }
	    else {
		
		    //Viva Colombia Arriba mxico
			banner1 = banner1 + expresion1.toUpperCase();  // a banner 1 le adiciono la expresin 2 (Arriba)
			banner1 = banner1 + " ";
			banner1 +=pais2;
			
			banner2 = banner2 + expresion2.substring(0,1);
			banner2 = banner2.toUpperCase();
			largo = expresion2.length();
			aux = expresion2.substring(1, largo);
			banner2 = banner2.concat(aux);
			banner2 = banner2 + " " + pais1;
	    }
	    
	    if (expresion3.equals("ABAJO")){
			banner = banner1 + " y " + banner2 + "\n";
			banner += banner2 + " y " + banner1;
			banner = banner.toUpperCase();
	    }
	    else{
		 	banner = banner1 + " y " + banner2 + "\n";
			banner += banner2 + " y " + banner1;
			banner = banner.toLowerCase();
	    }
	    
	    largo = banner.length();
	    letrero = banner.toCharArray();
	    
	    for(i=0; i<largo;i++) {
		System.out.print(letrero[i] + " ");
	    }
	    System.out.println();
	}
    }
}
