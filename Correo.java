package Laboratorio3;

import java.util.Scanner;

/**
 *
 * Correo
 *
 * Programa que genera correos electrónicos.<br>
 *
 */

public class Correo{

	/**
	 *
	 * Normaliza los parámetros y genera la dirección de correo.
	 *
	 * @param pn Primer nombre
	 * @param pa Primer apellido
	 * @param sa Segundo apellido
	 *
	 * @return Dirección de correo
	 *
	 */

	public static String generar(String pn, String pa, String sa){


		String username = "";
		username += pn.substring(0, 1).toLowerCase();

		if(pa.length() <= 6){
			username += pa.toLowerCase();
		}else{
			username += pa.substring(0, 6).toLowerCase();
		}

		username += sa.substring(0, 1).toLowerCase();
		username += "@sanmateo.edu.co";

		return username;
	}
	
	/**
	 * Verifica si el correo tiene caracteres inválidos.
	 * @param str correo electrónico.
	 * @return true si el correo tiene caracteres inválidos.
	 */

	public static boolean verificar(String str){

		boolean bool = false;
				for(int i = 0; i < str.length()-1; i++){
				switch (str.toLowerCase().charAt(i)){
					case 'á':
					case 'é':
					case 'í':
					case 'ó':
					case 'ú':
					case 'ñ':
					case 'ä':
					case 'ë':
					case 'ï':
					case 'ö':
					case 'ü':
						System.out.println("Caracter inválido");
						bool = true;
						break;

				}
			}

			return bool;
	}
	/**
	 *
	 * Método principal.<br>
	 *
	 * @param args Argumentos de línea de comandos.
	 *
	 */

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		do{
				String pn = "";
				String pa = "";
				String sa = "";

				do{
					
					System.out.print("\nPrimer nombre: ");
					pn = scan.nextLine();	
				}while(verificar(pn));
				

				do{
				System.out.print("Primer apellido: ");
				pa = scan.nextLine();
				}while(verificar(pa));

				do{
				System.out.print("Segundo apellido: ");
				sa = scan.nextLine();
				}while(verificar(sa));
		
		//	}

				System.out.println(generar(pn, pa, sa));
			System.out.print("¿Desea continuar? [S/n]: ");
			
		}while(scan.nextLine().equalsIgnoreCase("S"));
						scan.close();
}
}
