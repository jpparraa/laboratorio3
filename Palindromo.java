package Laboratorio3;


/**
 *
 * Programa que determina si una palabra o frase es palíndromo o no.
 *
 */ 
public class Palindromo{

	/**
	 *
	 * Normaliza el texto recibido.
	 *
	 * @param args Argumentos ingresados por la línea de comandos.
	 *
	 * @return Texto normalizado.
	 *
	 */

	public static String preparar(String[] args){

		String text1 = "";

		for(int i = 0; i < args.length; i++){
			text1 += args[i];
		}

		char[] cadena = text1.toCharArray();

		for(int i = 0; i < cadena.length; i++){
			switch(cadena[i]){
				case 'á':
					cadena[i] = 'a';
					break;
				case 'é':
					cadena[i] = 'e';
					break;
				case 'í':
					cadena[i] = 'i';
					break;
				case 'ó':
					cadena[i] = 'o';
					break;
				case 'ú':
					cadena[i] = 'u';
					break;

			}
		}

		String text = new String(cadena);

		return text;

	}

	/**
	 *
	 * Invierte el texto ingresado.
	 *
	 * @param text Texto normalizado.
	 *
	 * @return Texto invertido.
	 *
	 */

	public static String invertir(String text){
		
		String invert = "";
		
		for(int i = text.length() - 1; i >= 0; i--){
			invert += text.charAt(i);
		}

		return invert;
	}

	/**
	 *
	 * Determina si el texto es palíndromo o no.
	 *
	 * @param text Texto normalizado
	 * @param invert Texto invertido
	 *
	 */

	public static void comparar(String text, String invert){
		if(text.equalsIgnoreCase(invert)){
			System.out.println("Es un palíndromo.");
		}else{
			System.out.println("No es un palíndromo.");
		}
	}

	/**
	 *
	 * Programa principal
	 *
	 * @param args Argumentos de línea de comandos
	 *
	 */

	public static void main(String[] args){
		
		String text = "";
		if(args.length > 0){
			text = preparar(args);
			comparar(text, invertir(text));
		}else{
			System.out.println("Uso: java Palindromo {Texto}.");
		}
	}
}
