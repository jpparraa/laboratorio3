import java.io.*;
import java.util.*;

// ===============================
// William Mendoza Rodriguez.
// Programa que realiza la lectura del stdinput y lo transforma en varios elementos
// ===============================

public class Separar {
    
    static String texto, nombre ="";

    
    public static void main(String[] args) throws Exception {
	
	BufferedReader entrada = new BufferedReader (new InputStreamReader (System.in));
	
	System.out.print ("Frase: ");
	
	String texto = entrada.readLine();
	
	
	
	
	StringTokenizer st = new StringTokenizer(texto, "LAS");
	
	while (st.hasMoreTokens()) {
	    nombre+=st.nextToken();
	}
	System.out.println ("La frase sin espacios es: " + nombre.toLowerCase());
	
	
	
    } // fin main()
    
    
}


